#!/usr/bin/env bash

set -o errexit \
    -o pipefail

PLAINTEXT_DIR_NAME='plaintext'
export PLAINTEXT_DIR_NAME

SERVICE='lblval'
export SERVICE

NAMESPACE='infra'
export NAMESPACE

KUBECTL='microk8s.kubectl'
export KUBECTL
