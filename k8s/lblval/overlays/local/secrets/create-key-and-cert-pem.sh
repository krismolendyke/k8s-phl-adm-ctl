#!/usr/bin/env bash
# shellcheck disable=SC2002

set -o errexit \
    -o pipefail


cwd="${BASH_SOURCE%/*}"
if [[ ! -d "$cwd" ]]; then
    cwd="$PWD"
fi
# shellcheck disable=SC1090
source "${cwd}/common.sh"


# Private key
declare -r KEY_FILE="${PLAINTEXT_DIR_NAME}/${SERVICE}-key.pem"

# Certificate signing request configuration and file
declare -r CSR_CONF="${PLAINTEXT_DIR_NAME}/csr.conf"
declare -r CSR_FILE="${PLAINTEXT_DIR_NAME}/${SERVICE}.csr"

# CSR k8s object name and manifest file
declare -r CSR_NAME="${SERVICE}.${NAMESPACE}"
declare -r CSR_YAML="${PLAINTEXT_DIR_NAME}/csr.yaml"

# Signed certificate
declare -r CERT_FILE="${PLAINTEXT_DIR_NAME}/${SERVICE}-cert.pem"


if [ ! -x "$(command -v openssl)" ]; then
    echo 'openssl is required'
    exit 1
fi

mkdir -p "${PLAINTEXT_DIR_NAME}"


# Generate the private key for the webhook server
# genrsa  generate a private key
# -out    key file
# 2048    key size
openssl genrsa \
        -out "${KEY_FILE}" \
        2048

# Create the CSR conf file for creating the CSR
cat <<EOF > "${CSR_CONF}"
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
[req_distinguished_name]
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names
[alt_names]
DNS.1 = ${SERVICE}
DNS.2 = ${SERVICE}.${NAMESPACE}
DNS.3 = ${SERVICE}.${NAMESPACE}.svc
EOF

# Generate the CSR with the server's private key
# req      request a new certificate request
# -new     generate a new certificate request
# -key     file to read the private key from
# -subj    subject name
# -out     output file
# -config  config file
openssl req \
        -new \
        -key "${KEY_FILE}" \
        -subj "/CN=${SERVICE}.${NAMESPACE}.svc" \
        -out "${CSR_FILE}" \
        -config "${CSR_CONF}"


# Create k8s CSR manifest
cat <<EOF > "${CSR_YAML}"
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: ${CSR_NAME}
spec:
  groups:
  - system:authenticated
  request: $(cat ${CSR_FILE} | base64 | tr -d '\n')
  usages:
  - digital signature
  - key encipherment
  - server auth
EOF


if "${KUBECTL}" get csr "${CSR_NAME}" > /dev/null 2>&1; then
    echo
    echo "Certificate signing request '${CSR_NAME}' exists.  Delete and recreate?"
    select choice in 'Yes' 'No'; do
        case $choice in
            Yes ) "${KUBECTL}" delete csr "${CSR_NAME}"
                  break;;
            No )
                exit;;
        esac
    done
fi


echo
echo "Create certificate signing request '${CSR_NAME}'?"
"${KUBECTL}" create --dry-run --validate --filename "${CSR_YAML}" > /dev/null 2>&1
select choice in 'Yes' 'No'; do
    case $choice in
        Yes ) "${KUBECTL}" create --filename "${CSR_YAML}"
              break;;
        No )
            exit;;
    esac
done


echo
echo "Approve certificate signing request '${CSR_NAME}'?"
"${KUBECTL}" get csr "${CSR_NAME}"
select choice in 'Yes' 'No'; do
    case $choice in
        Yes ) "${KUBECTL}" certificate approve "${CSR_NAME}"
              break;;
        No )
            exit;;
    esac
done


# Get the signed certificate
sleep 3

signedCert=$("${KUBECTL}" get csr "${CSR_NAME}" --output go-template='{{ .status.certificate }}')
echo "${signedCert}" | openssl base64 -d -A -out "${CERT_FILE}"

echo
echo "Successfully created ${KEY_FILE} and ${CERT_FILE}"
