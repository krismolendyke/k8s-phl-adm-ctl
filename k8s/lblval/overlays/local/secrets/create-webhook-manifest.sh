#!/usr/bin/env bash

set -o errexit \
    -o pipefail


cwd="${BASH_SOURCE%/*}"
if [[ ! -d "$cwd" ]]; then
    cwd="$PWD"
fi
# shellcheck disable=SC1090
source "${cwd}/common.sh"


declare -r MANIFEST_FILE="${PLAINTEXT_DIR_NAME}/webhooks.yaml"


cat <<EOF > "${MANIFEST_FILE}"
apiVersion: admissionregistration.k8s.io/v1
kind: ValidatingWebhookConfiguration
metadata:
  name: ${SERVICE}
webhooks:
  - name: ${SERVICE}.${NAMESPACE}.svc
    clientConfig:
      service:
        name: ${SERVICE}
        namespace: ${NAMESPACE}
      caBundle: $(${KUBECTL} get configmap --namespace kube-system extension-apiserver-authentication --output go-template='{{ index .data "client-ca-file" }}' | base64 | tr -d '\n')
    rules:
      - operations:
          - "CREATE"
        apiGroups:
          - ""
        apiVersions:
          - "v1"
        resources:
          - "configmaps"
        scope: "Namespaced"
    admissionReviewVersions:
      - "v1"
    sideEffects: None
    namespaceSelector:
      matchExpressions:
        - key: lblval
          operator: In
          values:
            - "required"
EOF


echo "Successfully created ${MANIFEST_FILE}"
